

$("#form").submit(function (e) {
    e.preventDefault();

    document.getElementById("save").disabled = true;
    setTimeout(function () {
        document.getElementById("save").disabled = false;
    }, 5000);

    document.getElementById("save-container").style.display = "none";
    document.querySelector(".loader-holder").style.display = "block";

    var formObj = $(this);
    var formURL = formObj.attr("action");
    var formData = new FormData(this);
    console.log(formData);
    $.ajax({
        url: formURL,
        type: 'POST',
        data: formData,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        error: function (jqXHR) {

            var data = JSON.parse(jqXHR.responseText);
            const errors_container = $('#errors-container');
            errors_container.empty();

            errors_container.append('<center><h4 class="center">' + data["message"] + '</h4></center>');

            document.getElementById("save-container").style.display = "block";
            document.querySelector(".loader-holder").style.display = "none";

            document.getElementById('inscriere').scrollIntoView();
        },
        success: function (jqXHR) {
            var body = $('#form');

            $("#errors-container").empty();

            body.parent().append('<center><h1 style="color: white; padding-bottom: 61vh;">Felicitări! <br>Ai fost înregistrat cu succes!<br> Verifică-ți email-ul pentru a intra în posesia codurilor QR necesare în ziua evenimentului.</h1></center>');
            body.remove();
            document.querySelector('.navBar').scrollIntoView();
        }
    });
});
document.getElementById("regulament").setCustomValidity("Please indicate that you accept the Terms and Conditions");