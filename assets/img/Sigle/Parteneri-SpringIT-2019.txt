Ordinea: ING, Total Soft, KPMG, Sustain, Microsoft, American Greetings, HP, Metro Systems, Accenture, HTSS

ING: 
ING România face parte din grupul ING, instituție financiară globală care oferă servicii financiar-bancare. 
Clienții noștri sunt în centrul fiecărei decizii pe care o luăm, iar obiectivul nostru este să îi sprijinim să fie cu un pas înainte în viață și în afaceri.

 

ING România este reprezentat de 2 companii:

·         ING Bank România a fost înființată în 1994 și a făcut pionierat în industria bancară locală, 
lansând servicii și produse dedicate companiilor. În anul 2004 și-a extins serviciile către clienții individuali, 
printr-un model inovator de banking – ING Self’Bank. Astăzi, ING Bank România are peste 1,2 milioane de clienți activi, 
iar ING Home’Bank este cotată ca fiind cea mai bună aplicație de digital banking de pe piața locală.

·         ING Tech România este un hub global care dezvoltă soluții de software pentru întregul grup ING, 
în domenii precum Core Banking, Data Management și Wholesale Banking etc. Hub-ul de software a fost inaugurat în anul 2015 și, 
în prezent, lucrează cu 11 țări și este principalul furnizor de servicii IT al grupului ING.


Total Soft:

Toate companiile stiu CE fac si majoritatea stiu CUM fac asta. Insa foarte puține organizatii știu DE CE fac
ceea ce fac. De peste 25 de ani, TotalSoft se ghideaza dupa un scop, un DE CE clar: să construiasca soluții
care îi sprijină pe clienți să ofere noi tipuri de valoare pe piață, să creeze o nouă traiectorie de creștere
pentru afacerile lor.
In momentul de fata, TotalSoft este unul dintre cei mai importanți furnizori de sisteme informatice de
business (ERP, HCM, CRM, DM și BI) din Europa Centrală, cu proiecte in peste 40 de tari, dar si unul
dintre principalii dezvoltatori de software din Romania, cu o echipa de peste 500 de specialisti.
Produsele de top ale companiei, Charisma ERP, Charisma HCM și Charisma CRM, sunt lideri de piață în
România, cu aplicare în nouă verticale de business: financiar, retail, distribuție, producție, servicii,
construcții, agricultură, energie și medical. In plus, TotalSoft este unul dintre primii 10 furnizori globali
de soluții software pentru industria de leasing, conform studiului de piata redactat de PWC in 2017.
În 2016, compania TotalSoft a fost achiziționată de Logo Business Investment S.A. (Logo), cel mai mare
furnizor independent de software din Turcia, prezent pe 45 de piețe internaționale și deservind peste
85.000 de clienți din Europa, Orientul Mijlociu, Africa și Asia.

DE CE SA FII PARTE DIN CHARISMA ACADEMY
Charisma Academy este un program prin intermediul caruia TU si NOI crestem impreuna. Cum facem
asta?
Timp de trei luni, mentori bine pregatiti iti vor ghida primii pasii spre o cariera de succes. Alaturi de ei,
vei putea explora intregul ciclu de viata al unui produs software, de la etapa in care el este creat de catre
echipele de dezvoltatori pana in momentul in care clientii din diferite arii de business il utilizeaza. Vei lua
contact cu tehnologii variate (C#, .Net 4.5, ASP.Net/ASP .Net MVC, Javascript, jQuery, Oracle/SQL Server,
PLSQL/T-SQL), cu cele mai bune practici din domeniu si cu un portofoliu vast de companii lider in
industriile in care activeaza. Toate acestea se vor desfasura intr-un mediu relaxat si prietenos de
invatare si dezvoltare.
Vino cu energie si multa dorinta de cunoastere, cu idei inedite pe care sa le punem in practica impreuna,
iar la finalul celor trei luni poti sa iti alegi echipa alaturi de care vei face lucruri ce pot aduce valoare
business-ului clientilor nostri.
Te asteptam in echipa noastra!

KPMG:

KPMG in Romania &amp; Moldova is part of a global network of firms that offers Audit, Tax &amp; Legal,
Consulting, Deal Advisory and Technology services. With 5 offices across Romania &amp; Moldova, we
work with everyone from small start-ups and individuals to major multinationals, where we bring our
creativity and insight to our clients’ most critical challenges.
Trust is at the heart of everything we do. At KPMG we champion a vision of ‘Clear Choice’ – with
people who are extraordinary, with clients who see a difference in us and with trust from the public.
Our growth is driven by delivering real results for our clients. It&#39;s also enabled by our culture, which
encourages individual development, embraces an inclusive environment, rewards innovative
excellence and supports our communities.

Sustain:

De 25 de ani Sustainalytics este un important furnizor independent de cercetare în domeniul mediului, 
social și al guvernarii (ESG). Analizele realizate de colegii nostri sunt folosite de clienți pentru a lua decizii de investiții mai bine informate si 
pentru a putea dezvolta impreuna cu partenerii relatii care sa conduca la o schimbare in modul in care se fac afaceri la nivel global.

Sustainalytics este o organizatie in crestere avand peste 500 de angajati si 17 birouri la nivel global.
Cultura noastra este definita de flexibiliate si colaborare, angajatii nostri fiind incurajati sa performeze si 
sa se dezvolte intr-un mediu antreprenorial.

Microsoft:

Empower every person and organization on the planet to achieve more. That’s what
inspires us, drives our work and pushes us to challenge the status quo every day. At
Microsoft we also work to empower our employees so they can achieve more. We
believe we should each find meaning in our work and we ensure employees have the
freedom and the reach to help make a difference in the world.
Growth mindset: At Microsoft, we’re insatiably curious and always learning. We ask
questions, take risks and build on each other’s ideas, because we are better together.
We lean in to uncertainty, take risks and move quickly when we make mistakes,
because we know that failure happens along the way to innovation and breakthrough.
Customer obsessed: We are passionate about helping our customers achieve more,
and that means we really listen and learn from them. We bring solutions that don’t just
meet the needs of customers and their businesses, they often surprise and delight
them. Then we innovate further to give them even more.
Diversity and inclusion: We don’t just value differences, we seek them out. We invite
them in. Microsoft is a place where employees can be who they are. We value diverse
perspectives. And as a result, we have better ideas, better products and happier
customers.
One Microsoft: We are a family of individuals at a truly global company, united by a
single mission. We work together, building on each other’s ideas and collaborating
across boundaries to bring the best of Microsoft to our customers and the world.
Making a difference: Our employees have access to the latest technology and tools,
the power to build on the company’s far-reaching momentum and the drive to change
the world. We can make a difference. Together, we can help billions of people around
the globe use digital technology to achieve amazing things.
We know where we’re going as a company and how we’ll get there. We are committed
to reinvent productivity and business processes, build the intelligent cloud platform and
create more personal computing. These three goals are interconnected, they’re
bold—and they’re why we are always looking to bring even more brilliant and creative
people to our team.

American Greetings:

American Greetings – Romania este un centru de livrare de servicii IT furnizate companiei mama, divizia
AG Digital, acoperind urmatoarele arii: Dezvoltare Aplicatii Mobile si Web, Infrastructura, Baze de date,
Testare manuala si automata.
Internship @AG
Un program cu o structura complexa, ce include training in departamentele - de Dezvoltare
Mobile si Web, Infrastructura, Baze de date, Testare - si experimentarea tehnologiilor specifice fiecarui
sector IT. La finalul programului vei avea o viziune completa despre ceea ce implica dezvoltarea de site,
dezvoltarea de aplicatii mobile, administrarea de baze de date, infrastructura: securitate, retea, server,
si solutii optime in tesarea automata si manuala.
Programul include 6 saptamani de training si 7 saptamani de practica, prin implicarea in proiecte
de lucru reale.
Doresti sa te afirmi intr-o companie care sa te ajute sa te formezi si sa te dezvolti? Alatura-te echipei
noastre!
Aplica la Internship AG sau trimite CV-ul tau la adresa: cariere.ro@ag.com 

Prezentare companie pentru pagina Facebook:
American Greetings – Romania este un centru de servicii IT furnizate companiei mama, AG
Digital, pentru urmatoarele divizii: Dezvoltare Aplicatii Mobile si Web, Infrastructura, Baze de
date, Testare manuala si automata.
Pentru mai multe despre noi, joburi, oportunitati de dezvoltare, oameni, atmosfera de lucru si
orice alta informatie relevanta pentru tine, te asteptam cu drag in data de 4 martie 2019 la
prezentarea companiei din cadrul Conferintei de deschidere a evenimentului Spring IT .

Hewlett Packard(HP):

Hewlett Packard Enterprise este unul din liderii globali in industria IT, avand HQs in Palo Alto California si 
dezvoltand tehnologii de servere, storage, cloud, networking si converged systems care sunt sustinute de servicii de 
consultanta, suport, core SW si servicii financiare. La acestea se adauga si AI, Robotics si Automation.
Desi incepand cu 1 noiembrie 2015, structura initiala a companiei a trecut printr-un process de transformare si astfel 
a fost infiintat HPE, continuam sa avem un focus deosebit pe dezvoltarea profesionala si personala a angajatilor nostri, 
cat si pe idea de comunitate creata in fiecare centru.

MetroSystems:
METRO SYSTEMS Romania este subsidiara companiei METRO SYSTEMS Germania care ofera solutii IT complexe pentru industria de retail. 
METRO SYSTEMS Romania ofera solutii de Design, Quality Assurance, Support, Delivery & Deployment, Application Operation and Infrastructure Operation. 
METRO SYSTEMS GmbH asigura servicii de management al informatiilor si tehnologiilor pentru METRO. 
METRO SYSTEMS dezvolta si implementeaza solutii de management al marfurilor si logisticii, solutii pentru gestionarea si arhivarea informatiilor, 
sisteme de baze de date cu informatii despre clienti, sisteme de case de marcat, solutii e-commerce si sisteme de intranet, precum si sisteme administrative. 
Din anul 2006, anul înfiinţării METRO SYSTEMS Romania, numarul de angajați a crescut constant pana la mai mult de 800, iar compania continuă să se extindă. 
METRO este una dintre cele mai mari și cele mai importante companii comerciale internaționale. 
Compania opereaza in 35 de tari si are un numar mai mare de 150.000 de angajați.

Accenture: 

Accenture is a leading global professional services company, providing a broad range of services and solutions in
strategy, consulting, digital, technology and operations. We work at the intersection of business and technology
to help clients improve their performance and create sustainable value. With more than 449,000 people serving
clients in more than 120 countries, we drive innovation to improve the way the world works and lives.
At Accenture Technology, we investigate and explore the new technologies on the market, to
determine how our clients can use them – helping their businesses innovate, grow and improve.
Here, we innovate with the newest technologies.
What’s in it for you?
Accenture Technology will allow you to be part of the New. Now and change the world with New IT.
Unleash your skills and transform the world around you. Shape the technology of tomorrow, with the latest digital
methodologies and design thinking, like cloud, AI, intelligent automation, DevOps and Agile. Implement
innovative solutions to help clients drive disruption and stay ahead of the digital curve.
01 Build bold new business models via application management and implementation services.
02 Create and deliver custom-designed solutions for our clients&#39; most complex technology challenges.
03 Leverage our alliances with leading companies - such as SAP, Oracle, Microsoft, Salesforce, Cisco, HP and
IBM – to drive and accelerate real business impact.
04 Incubate and pilot the next wave of emerging technologies for clients based on our applied R&amp;D.
Our people
Individuality, authenticity and inclusivity are core aspects of our culture. At Accenture, we value the passion and
strengths of our employees and help them achieve high performance in their careers.
Our office
Our Technology Romania office is located in Bucharest, Westgate Office Park. Find out more about Accenture
Technology opportunities, on accenture.ro/TechnologyCareer.
Find the latest news and information about Accenture in general and get in touch with us on our Facebook Page -
Accenture in Romania or on our website - accenture.ro/cariere.

HTSS:

Fondată în anul 2012, High-Tech Systems &amp; Software oferă solutii software la nivel mondial, dar și
servicii, sisteme hardware și de infrastructură. Cu o experiență bine înrădăcinată în dezvoltare
software, am dezvoltat soluții și le-am crescut dinamic în zone precum WMS, ERP, Sales Force
Automation, CRM, Data Analysis, Hospital Information System, Securitatea Informației și multe
altele. De asemenea, îi avem alături, în calitate de parteneri, pe Microsoft, IBM, HPE, Xerox, McAfee,
Symantec și mulți alți lideri care ne ajută să le aducem clienților noștri beneficiile de care au nevoie.



